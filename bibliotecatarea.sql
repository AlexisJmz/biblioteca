//TORAL ENRIQUEZ LUZCEILA//

drop database if exists biblioteca;
create database biblioteca;
use biblioteca;

create table escuelas(
id int not null auto_increment primary key,
nombreEsc varchar (100) not null
);

create table estudiantes(
id int not null auto_increment primary key,
nombreEst varchar (100) not null,
apellidos varchar (100) not null,
edad varchar (10) not null,
sexo varchar (10) not null,
tipo varchar(20) not null,
escuela_id int not null,
FOREIGN KEY(escuela_id) REFERENCES escuelas(id)
on delete restrict on update cascade
);

create table servicios(
id int auto_increment primary key,
nombreSer varchar (100) not null,
descripcion varchar(100) not null
);

create table estudiantes_servicios(
id int auto_increment primary key,
estudiante_id int not null,
servicio_id int not null,
numMaquina int(3) not null,
fecha date  not null,
hora_inicio time not null,
hora_final time not null,
FOREIGN KEY(estudiante_id) REFERENCES estudiantes(id)
on delete restrict on update cascade,
FOREIGN KEY(servicio_id) REFERENCES servicios(id)
on delete restrict on update cascade
);
