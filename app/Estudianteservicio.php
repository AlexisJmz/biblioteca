<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudianteservicio extends Model
{
    protected $table="estudiantes_servicios";
    protected $fillable = ['id','estudiante_id','servicio_id','numMaquina','fecha','hora_inicio','hora_final'];
    public $timestamps = false;
    public function estudiante(){
        return $this->belongsTo(Estudiante::class,'estudiante_id');
    }

    public function servicio(){
        return $this->belongsTo(Servicio::class,'servicio_id');
    }
}
