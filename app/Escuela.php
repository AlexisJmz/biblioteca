<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escuela extends Model
{
    protected $table =  "escuelas";

    protected $fillable = ['id','nombreEsc'];
    public $timestamps = false;

    public function estudiantes(){
        return $this->hasMany(Estudiante::class);
    }
}
