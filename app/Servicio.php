<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
    protected $table= "servicios";
    protected $fillable = ['id','nombreSer','descripcion'];
    public $timestamps = false;

    public function estudianteservicios(){
        return $this->hasMany(Estudianteservicio::class);
    }
}
