<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    protected $table =  "estudiantes";

    protected $fillable = ['id','nombreEst','edad','sexo','tipo','escuela_id'];
    public $timestamps = false;

    public function escuela(){
      return $this->belongsTo(Escuela::class,'escuela_id');
    }

    public function estudianteservicios(){
      return $this->hasMany(Estudianteservicio::class);
    }
}
