<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servicio as Servicio;
class ServicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicios = Servicio::all();
        return view('sistema.servicios.index',compact('servicios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sistema.servicios.agregar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'nombreSer'=>'required|unique:servicios,nombreSer',
            'descripcion'=>'required'
        ],[
            'nombreSer.required'=>'EL nombre del servicio debe ser requerido',
            'nombreSer.unique'=>'EL nombre del servicio debe ser unico',
            'descripcion.required' =>'La descripcion debe ser requerida'
        ]);

        $servicio = new Servicio;
        $servicio->nombreSer = $data['nombreSer'];
        $servicio->descripcion = $data['descripcion'];
        $alert="success";
		if($servicio->save()){
			$dato="Se guardo el servicio";
		}else{
			$dato="No pudo guardar el servicio";
			$alert="danger";
		}

		return back()->with('dato',$dato)->with('alert',$alert);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicio = Servicio::findOrFail($id);
        return view('sistema.servicios.editar',compact('servicio')); 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = request()->validate([
            'nombreSer'=>'required',
            'descripcion'=>'required'
        ],[
            'nombreSer.required'=>'EL nombre del servicio debe ser requerido',
            'descripcion.required' =>'La descripcion debe ser requerida'
        ]);
        
        $servicio = Servicio::findOrFail($id);
        $servicio->nombreSer = $data['nombreSer'];
        $servicio->descripcion = $data['descripcion'];
        $alert="success";
		if($servicio->save()){
			$dato="Se edito el servicio";
		}else{
			$dato="No pudo editar el servicio";
			$alert="danger";
		}

		return back()->with('dato',$dato)->with('alert',$alert);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $servicio = Servicio::findOrFail($id);
        $alert="success";
		if($servicio->delete()){
			$dato="Se elimino el servicio";
		}else{
			$dato="No pudo eliminar el servicio";
			$alert="danger";
		}

		return back()->with('dato',$dato)->with('alert',$alert);
    }
}
