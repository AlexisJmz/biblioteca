<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estudianteservicio as Estudianteservicio;
use App\Servicio as Servicio;
use App\Estudiante as Estudiante;
class EstudianteServicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $asignaciones = Estudianteservicio::orderBy('id','desc')->get();
        return view('sistema.asignaciones.index',compact('asignaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $servicios = Servicio::all();
        $estudiantes = Estudiante::all();
        return view('sistema.asignaciones.agregar',compact(['servicios','estudiantes']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'estudiante_id'=>'required',
            'servicio_id'=>'required',
            'numMaquina'=>'required|numeric'
        ],[
            'estudiante_id.required'=>'El estudiante es requerido',
            'servicio_id.required'=>'El servicio es requerido',
            'numMaquina.required'=>'El numero de maquina es requerido'
        ]);
        $asignacion = new Estudianteservicio;
        $asignacion->estudiante_id = $data['estudiante_id'];
        $asignacion->servicio_id = $data['servicio_id'];
        $asignacion->numMaquina = $data['numMaquina'];
        $asignacion->fecha = date('Y-m-d');
        $asignacion->hora_inicio = date('H:i:s');
        $asignacion->hora_final = '00:00:00';
        $alert="success";
        $numMaquinaOcupado = Estudianteservicio::where('numMaquina','=',$data['numMaquina'])->where('hora_final','=','00:00:00')->count();
        $estudianteOcupado = Estudianteservicio::where('estudiante_id','=',$data['estudiante_id'])->where('hora_final','=','00:00:00')->count();
        if($numMaquinaOcupado > 0 || $estudianteOcupado > 0){
            $dato = "Estudiante o maquina esta en uso!";
            $alert = "danger";
        }else{
            if($asignacion->save()){
                $dato="Se guardo la asignacion";
            }else{
                $dato="No pudo guardar asignacion";
                $alert="danger";
            }    
        }
        return back()->with('dato',$dato)->with('alert',$alert); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asignacion= Estudianteservicio::findOrFail($id);
        $servicios = Servicio::all();
        $estudiantes = Estudiante::all();
        return view('sistema.asignaciones.editar',compact(['servicios','estudiantes','asignacion']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $asignacion = Estudianteservicio::findOrFail($id);
        $asignacion->hora_final = date('H:i:s');
        $alert="success";
        if($asignacion->save()){
            $dato="Se asigno la hora final";
        }else{
            $dato="No pudo asignar la hora final";
            $alert="danger";
        } 
        return back()->with('dato',$dato)->with('alert',$alert);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
