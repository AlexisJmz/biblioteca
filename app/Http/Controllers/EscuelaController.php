<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Escuela as Escuela;

class EscuelaController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$escuelas = Escuela::all();
		return view('sistema.escuelas.index',compact('escuelas'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('sistema.escuelas.agregar');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		$data = request()->validate([
				'nombreEsc' => 'required|unique:escuelas,nombreEsc',
				
		], [
			'nombreEsc.required' => 'El campo nombre de la escuela es obligatorio',
			'nombreEsc.unique' => 'El nombre de la escuela ya esta en uso'
		]);
		$escuela = new Escuela;
		$escuela->nombreEsc = $data['nombreEsc'];
		$alert="success";
			if($escuela->save()){
				$dato="Se guardo la escuela";
			}else{
				$dato="No pudo guardar la escuela";
				$alert="danger";
			}

			return back()->with('dato',$dato)->with('alert',$alert);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$escuela = Escuela::findOrFail($id);
		return view('sistema.escuelas.editar',compact('escuela'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$data = request()->validate([
				'nombreEsc' => 'required',
				
		], [
			'nombreEsc.required' => 'El campo nombre de la escuela es obligatorio',
			
		]);
		$escuela = Escuela::findOrFail($id);
		$escuela->nombreEsc = $data['nombreEsc'];
		
		$alert="success";
			if($escuela->save()){
				$dato="Se edito la escuela";
			}else{
				$dato="No pudo editar la escuela";
				$alert="danger";
			}

			return back()->with('dato',$dato)->with('alert',$alert);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$escuela = Escuela::findOrFail($id);
		$alert="success";
			if($escuela->delete()){
				$dato="Se elimino la escuela";
			}else{
				$dato="No pudo eliminar la escuela";
				$alert="danger";
			}

			return back()->with('dato',$dato)->with('alert',$alert);
	}
}
