<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estudiante as Estudiante;
use App\Escuela as Escuela;
class EstudianteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estudiantes = Estudiante::all();
        return view('sistema.estudiantes.index',compact('estudiantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $escuelas = Escuela::all();
        return view('sistema.estudiantes.agregar',compact('escuelas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
            'nombreEst'=>'required',
            'apellidos'=> 'required',
            'edad'=>'required|numeric',
            'sexo'=>'required',
            'tipo'=>'required',
            'escuela_id'=>'required'
        ],[
            'nombreEst.required'=>'El nombre es requerido',
            'apellidos.required'=>'El apellido es requerido',
            'edad.required'=>'La edad es requerida',
            'edad.numeric'=>'La edad debe ser numerico',
            'sexo.required'=>'El sexo es requerido',
            'tipo.required'=>'El tipo es requerido',
            'escuela_id.required'=>'La escuela es requerido',
        ]);

        $estudiante = new Estudiante;
        $estudiante->nombreEst = $data['nombreEst'];
        $estudiante->apellidos = $data['apellidos'];
        $estudiante->edad = $data['edad'];
        $estudiante->sexo = $data['sexo'];
        $estudiante->tipo = $data['tipo'];
        $estudiante->escuela_id = $data['escuela_id'];
        
        $alert="success";
		if($estudiante->save()){
			$dato="Se guardo el estudiante";
		}else{
			$dato="No pudo guardar el estudiante";
			$alert="danger";
		}

		return back()->with('dato',$dato)->with('alert',$alert);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $estudiante =  Estudiante::findOrFail($id);
        $escuelas = Escuela::all();
        return view('sistema.estudiantes.editar',compact(['estudiante','escuelas']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = request()->validate([
            'nombreEst'=>'required',
            'apellidos'=> 'required',
            'edad'=>'required|numeric',
            'sexo'=>'required',
            'tipo'=>'required',
            'escuela_id'=>'required'
        ],[
            'nombreEst.required'=>'El nombre es requerido',
            'apellidos.required'=>'El apellido es requerido',
            'edad.required'=>'La edad es requerida',
            'edad.numeric'=>'La edad debe ser numerico',
            'sexo.required'=>'El sexo es requerido',
            'tipo.required'=>'El tipo es requerido',
            'escuela_id.required'=>'La escuela es requerido',
        ]);

        $estudiante = Estudiante::findOrFail($id);
        $estudiante->nombreEst = $data['nombreEst'];
        $estudiante->apellidos = $data['apellidos'];
        $estudiante->edad = $data['edad'];
        $estudiante->sexo = $data['sexo'];
        $estudiante->tipo = $data['tipo'];
        $estudiante->escuela_id = $data['escuela_id'];
        
        $alert="success";
		if($estudiante->save()){
			$dato="Se edito el estudiante";
		}else{
			$dato="No pudo editar el estudiante";
			$alert="danger";
		}

		return back()->with('dato',$dato)->with('alert',$alert);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estudiante = Estudiante::findOrFail($id);
		$alert="success";
			if($estudiante->delete()){
				$dato="Se elimino el estudiante";
			}else{
				$dato="No pudo eliminar el estudiante";
				$alert="danger";
			}

			return back()->with('dato',$dato)->with('alert',$alert);
    }
}
