@extends('layouts.app')
@section('title','Listado servicios')
@section('content')
<div class="col-sm-12 col-md-12 col-xs-12 mx-auto">
	<div class="card">
		<div class="card-header">
			<h1 class="text-center text-success">Listado de servicios</h1>
			<a class="btn btn-success btn-block" href="{{route('servicios.create')}}">Agregar <span
					class="mdi mdi-plus"></span></a>
		</div>
		<div class="card-body">
		@include('layouts.info')
			<table class="table table-hover ">
				<thead>
					<tr>
						<th scope="col">id</th>
						<th scope="col">Nombre del servicio</th>
						<th scope="col">Descripcion</th>
						<th scope="col" colspan="2" class="text-center">Operaciones</th>
					</tr>
				</thead>
				<tbody>
					
					@foreach($servicios as $s)
					<tr>
							<td>{{$s->id}}</td>
							<td>{{$s->nombreSer}}</td>
							<td>{{$s->descripcion}}</td>
							<td><a href="{{route('servicios.edit',$s->id)}}" class="btn btn-warning">Editar</a></td>
							<td>
							{!! Form::open(['route' => ['servicios.destroy',$s->id],'method'=>'DELETE']) !!}
								<button type="submit" class="btn btn-danger">Eliminar</button>
							{!! Form::close() !!}
							</td>
					</tr>			
					@endforeach
					
				</tbody>

			</table>
			
		</div>
	</div>
</div>
@endsection
