<div class="row">
	
    <div class="form-group col-sm-8 col-md-8 col-xs-8 mx-auto">
      {!! Form::label('nombreSer','Nombre del Servicio:', ['class' => 'text-primary']);!!}
      {!! Form::text('nombreSer',old('nombreSer'), ['class' => 'form-control ',
      'name'=>'nombreSer','id'=>'nombreSer']);!!}
      
    </div>
    <div class="form-group col-sm-8 col-md-8 col-xs-8 mx-auto">
      {!! Form::label('descripcion','Descripcion:', ['class' => 'text-primary']);!!}
      {!! Form::textarea('descripcion',old('descripcion'), ['class' => 'form-control ',
      'name'=>'descripcion','id'=>'descripcion']);!!}
      
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-4 col-md-4 col-xs-4 mx-auto my-4">
      <button type="submit" class="btn btn-primary btn-lg btn-block">Aceptar</button>
    </div>
</div>




