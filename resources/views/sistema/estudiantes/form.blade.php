<div class="form-group col-sm-12 col-md-12 col-xs-12 mx-auto">
  <div class="row">
        <div class="form-group col-sm-4 col-md-4 col-xs-4 ">
          {!! Form::label('nombreEst','Nombre del estudiante:', ['class' => 'text-primary']);!!}
          {!! Form::text('nombreEst',old('nombreEst'), ['class' => 'form-control ',
          'name'=>'nombreEst','id'=>'nombreEst']);!!}
          
        </div>
        <div class="form-group col-sm-4 col-md-4 col-xs-4 ">
          {!! Form::label('apellidos','Apellidos:', ['class' => 'text-primary']);!!}
          {!! Form::text('apellidos',old('apellidos'), ['class' => 'form-control ',
          'name'=>'apellidos','id'=>'apellidos']);!!}
          
        </div>
        <div class="form-group col-sm-4 col-md-4 col-xs-4">
          {!! Form::label('edad','Edad:', ['class' => 'text-primary']);!!}
          {!! Form::text('edad',old('edad'), ['class' => 'form-control ',
          'name'=>'edad','id'=>'edad']);!!}    
        </div>
  </div>
</div>  
<div class="form-group col-sm-12 col-md-12 col-xs-12 mx-auto">
  <div class="row">
    <div class="form-group col-sm-4 col-md-4 col-xs-4 mx-auto">
      {!! Form::label('sexo','Sexo:', ['class' => 'text-primary']);!!}
      {!! Form::select('sexo', ['Hombre' => 'Hombre', 'Mujer' => 'Mujer'], null, ['placeholder' => 'Seleccione por favor','class'=>'form-control']);!!}
      
    </div>
    <div class="form-group col-sm-4 col-md-4 col-xs-4 mx-auto">
      {!! Form::label('tipo','Tipo:', ['class' => 'text-primary']);!!}
      {!! Form::select('tipo', ['Escolarizado' => 'Escolarizado', 'Sabatino' => 'Sabatino'], null, ['placeholder' => 'Seleccione por favor','class'=>'form-control']);!!}
      
    </div>
    <div class="form-group col-sm-4 col-md-4 col-xs-4 mx-auto">
      {!! Form::label('nombreEsc','Nombre de la escuela:', ['class' => 'text-primary']);!!}
      @if(isset($estudiante))
      <select class="form-control" name="escuela_id" id="escuela_id">
        <option value="">Seleccione escuela por favor..</option>
        @foreach ($escuelas as $esc)
        <option value="{{$esc->id}}" {{ $estudiante->escuela_id == $esc->id ? 'selected':''}}>
          {{$esc->nombreEsc}}</option>

        @endforeach
      </select>
      @else
      <select class="form-control" name="escuela_id" id="escuela_id">
        <option value="">Seleccione escuela por favor..</option>
        @foreach ($escuelas as $esc)
        <option value="{{$esc->id}}" {{ old('escuela_id') == $esc->id ? 'selected' : ''}}>
          {{$esc->nombreEsc}}</option>

        @endforeach
      </select>
      @endif

      
    </div>
  </div>
</div>      

<div class="row">
  <div class="form-group col-sm-4 col-md-4 col-xs-4 mx-auto my-4">
    <button type="submit" class="btn btn-primary btn-lg btn-block">Aceptar</button>
  </div>
</div>




