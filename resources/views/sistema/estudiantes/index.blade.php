@extends('layouts.app')
@section('title','Listado Estudiantes')
@section('content')
<div class="col-sm-12 col-md-12 col-xs-12 mx-auto">
	<div class="card">
		<div class="card-header">
			<h1 class="text-center text-success">Listado de Estudiantes</h1>
			<a class="btn btn-success btn-block" href="{{route('estudiantes.create')}}">Agregar <span
					class="mdi mdi-plus"></span></a>
		</div>
		<div class="card-body">
		@include('layouts.info')
			<table class="table table-hover ">
				<thead>
					<tr>
						<th scope="col">id</th>
						<th scope="col">Nombre completo</th>
						<th scope="col">Edad</th>
						<th scope="col">Sexo</th>
						<th scope="col">Tipo</th>
						<th scope="col">Nombre de la Escuela</th>
						<th scope="col" colspan="2" class="text-center">Operaciones</th>
					</tr>
				</thead>
				<tbody>
					
					@foreach($estudiantes as $e)
					<tr>
							<td>{{$e->id}}</td>
							<td>{{$e->nombreEst}} {{$e->apellidos}}</td>
							<td>{{$e->edad}}</td>
							<td>{{$e->sexo}}</td>
							<td>{{$e->tipo}}</td>
							<td>{{$e->escuela->nombreEsc}}</td>
							<td><a href="{{route('estudiantes.edit',$e->id)}}" class="btn btn-warning">Editar</a></td>
							<td>
							{!! Form::open(['route' => ['estudiantes.destroy',$e->id],'method'=>'DELETE']) !!}
								<button type="submit" class="btn btn-danger">Eliminar</button>
							{!! Form::close() !!}
							</td>
					</tr>			
					@endforeach
					
				</tbody>

			</table>
			
		</div>
	</div>
</div>
@endsection
