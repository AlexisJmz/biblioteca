@extends('layouts.app')
@section('title','Editar estudiante')
@section('content')

<div class="col-sm-12 col-md-12 col-xs-12 mx-auto">
	<div class="card">
		<div class="card-header text-center">
			<h1 class="text-center text-primary">Editar estudiante</h1>
		
		</div>
		<div class="card-body">
		@include('layouts.info')
			{!! Form::model($estudiante, ['route' => ['estudiantes.update',$estudiante->id],'method' => 'PATCH']) !!}
			@include('sistema.estudiantes.form')
			{!! Form::close() !!}
		</div>
		
	</div>
</div>

@endsection