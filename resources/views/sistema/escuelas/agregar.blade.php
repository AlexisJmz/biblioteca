@extends('layouts.app')
@section('title','Agregar escuela')
@section('content')

<div class="col-sm-12 col-md-12 col-xs-12 mx-auto">
	<div class="card">
		<div class="card-header text-center">
			<h1 class="text-center text-primary"> Agregar escuela</h1>
      
		</div>
    @include('layouts.info')
		<div class="card-body">
			{!! Form::open(['route' => 'escuelas.store']) !!}
			@include('sistema.escuelas.form')
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endsection