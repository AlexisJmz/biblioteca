@extends('layouts.app')
@section('title','Listado escuelas')
@section('content')
<div class="col-sm-12 col-md-12 col-xs-12 mx-auto">
	<div class="card">
		<div class="card-header">
			<h1 class="text-center text-success">Listado de Escuelas</h1>
			<a class="btn btn-success btn-block" href="{{route('escuelas.create')}}">Agregar <span
					class="mdi mdi-plus"></span></a>
		</div>
		<div class="card-body">
		@include('layouts.info')
			<table class="table table-hover ">
				<thead>
					<tr>
						<th scope="col">id</th>
						<th scope="col">Nombre de la escuela</th>
						
						<th scope="col" colspan="2" class="text-center">Operaciones</th>
					</tr>
				</thead>
				<tbody>
					
					@foreach($escuelas as $e)
					<tr>
							<td>{{$e->id}}</td>
							<td>{{$e->nombreEsc}}</td>
							<td><a href="{{route('escuelas.edit',$e->id)}}" class="btn btn-warning">Editar</a></td>
							<td>
							{!! Form::open(['route' => ['escuelas.destroy',$e->id],'method'=>'DELETE']) !!}
								<button type="submit" class="btn btn-danger">Eliminar</button>
							{!! Form::close() !!}
							</td>
					</tr>			
					@endforeach
					
				</tbody>

			</table>
			
		</div>
	</div>
</div>
@endsection
