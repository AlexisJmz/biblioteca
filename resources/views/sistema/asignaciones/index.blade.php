@extends('layouts.app')
@section('title','Listado asignaciones')
@section('content')
<div class="col-sm-12 col-md-12 col-xs-12 mx-auto">
	<div class="card">
		<div class="card-header">
			<h1 class="text-center text-success">Listado de asignaciones</h1>
			<a class="btn btn-success btn-block" href="{{route('asignaciones.create')}}">Asignar estudiante - servicio<span
					class="mdi mdi-plus"></span></a>
		</div>
		<div class="card-body">
		@include('layouts.info')
			<table class="table table-hover ">
				<thead>
					<tr>
						<th scope="col">id</th>
						<th scope="col">Estudiante</th>
						<th scope="col">Servicio</th>
						<th scope="col">Numero Maquina</th>
						<th scope="col">Fecha</th>
						<th scope="col">Hora inicial</th>
						<th scope="col">Hora final</th>
						<th scope="col"  class="text-center">Operaciones</th>
					</tr>
				</thead>
				<tbody>
					
					@foreach($asignaciones as $a)
					<tr>
							<td>{{$a->id}}</td>
							<td>{{$a->estudiante->nombreEst}} {{$a->estudiante->apellidos}} </td>
							<td>{{$a->servicio->nombreSer}}</td>
							<td>{{$a->numMaquina}}</td>
							<td>{{$a->fecha}}</td>
							<td>{{$a->hora_inicio}}</td>
							<td class="{{($a->hora_final == '00:00:00')? 'bg-danger text-warning':''}}">{{$a->hora_final}}</td>
							<td>
							@if($a->hora_final == '00:00:00')
								{!! Form::open(['route' => ['asignaciones.update',$a->id],'method' => 'PATCH']) !!}
									<button type="submit" class="btn btn-warning">Deteneter</button>
								{!! Form::close() !!}
							@endif
							

						
					</tr>			
					@endforeach
					
				</tbody>

			</table>
			
		</div>
	</div>
</div>
@endsection
