@extends('layouts.app')
@section('title','Editar asignacion')
@section('content')

<div class="col-sm-12 col-md-12 col-xs-12 mx-auto">
	<div class="card">
		<div class="card-header text-center">
			<h1 class="text-center text-primary">Editar asignacion</h1>
		
		</div>
		<div class="card-body">
		@include('layouts.info')
			{!! Form::model($asignacion, ['route' => ['asignaciones.update',$asignacion->id],'method' => 'PATCH']) !!}
			@include('sistema.asignaciones.form')
			{!! Form::close() !!}
		</div>
		
	</div>
</div>

@endsection