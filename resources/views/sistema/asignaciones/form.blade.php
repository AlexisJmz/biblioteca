<div class="form-group col-sm-12 col-md-12 col-xs-12 mx-auto">
  <div class="row">
        <div class="form-group col-sm-4 col-md-4 col-xs-4 ">
        {!! Form::label('estudiante_id','Nombre del alumno:', ['class' => 'text-primary']);!!}
        @if(isset($asignacion))
        <select class="form-control" name="estudiante_id" id="estudiante_id">
          <option value="">Seleccione alumno por favor por favor..</option>
          @foreach ($estudiantes as $est)
          <option value="{{$est->id}}" {{ $asignacion->estudiante_id == $est->id ? 'selected':''}}>
            {{$est->nombreEst}}  {{$est->apellidos}} - {{$est->escuela->nombreEsc}}</option>

          @endforeach
        </select>
        @else
        <select class="form-control" name="estudiante_id" id="estudiante_id">
          <option value="">Seleccione estudiante por favor..</option>
          @foreach ($estudiantes as $est)
          <option value="{{$est->id}}" {{ old('estudiante_id') == $est->id ? 'selected' : ''}}>
            {{$est->nombreEst}} {{$est->apellidos}} - {{$est->escuela->nombreEsc}}</option>

          @endforeach
        </select>
        @endif

        </div>
        <div class="form-group col-sm-4 col-md-4 col-xs-4 ">
        {!! Form::label('servicio_id','Servicio:', ['class' => 'text-primary']);!!}
          @if(isset($asignacion))
          <select class="form-control" name="escuela_id" id="escuela_id">
            <option value="">Seleccione servicio por favor..</option>
            @foreach ($servicios as $ser)
            <option value="{{$ser->id}}" {{ $asignacion->servicio_id == $ser->id ? 'selected':''}}>
              {{$ser->nombreSer}}</option>

            @endforeach
          </select>
          @else
          <select class="form-control" name="servicio_id" id="servicio_id">
            <option value="">Seleccione servicio por favor..</option>
            @foreach ($servicios as $ser)
            <option value="{{$ser->id}}" {{ old('servicio_id') == $ser->id ? 'selected' : ''}}>
              {{$ser->nombreSer}}</option>

            @endforeach
          </select>
          @endif

        </div>
        <div class="form-group col-sm-4 col-md-4 col-xs-4 mx-auto">
          {!! Form::label('numMaquina','numMaquina:', ['class' => 'text-primary']);!!}
          {!! Form::select('numMaquina', ['1'=>'1','2'=>'2','3'=>'3','4'=>'4','5'=>'5','6'=>'6','7'=>'7','8'=>'8','9'=>'9','10'=>'10','11'=>'11','12'=>'12'], null, ['placeholder' => 'Seleccione por favor','class'=>'form-control']);!!}
          
        </div>
  </div>
</div>  

<div class="row">
  <div class="form-group col-sm-4 col-md-4 col-xs-4 mx-auto my-4">
    <button type="submit" class="btn btn-primary btn-lg btn-block">Aceptar</button>
  </div>
</div>




