@if(Session::has('dato') || Session::has('alert') )
<div class="container my-2">
	<div class="alert alert-{{Session::get('alert')}} col-md-7 offset-md-2"  >
		{{ Session::get('dato') }}
	</div>

</div>
@endif
@if($errors->any())
<div class="shadow alert alert-danger" >
	@if($errors->any())
	<ul>
		@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
		@endforeach
	</ul>
	@endif
</div>
@endif



<script>
	$('.alert').slideDown();
	setTimeout(()=> $('.alert').slideUp(),10000)
</script>
